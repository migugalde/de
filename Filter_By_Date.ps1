﻿#Set-ExecutionPolicy Unrestricted
$ErrorActionPreference = "silentlycontinue"
# Function to recursively delete files and folders older than $LIMIT starting at an absolute directory $PATH.
Function FilterByDate ($directory, $limit) {

# Delete files @ $PATH older than $limit
Get-ChildItem -Path $directory -Recurse -Force| Where-Object { !$_.PSIsContainer -and $_.CreationTime -lt $limit } | Remove-Item -Recurse

# Delete directrories which are a) empty and b) older than the $limit
Get-ChildItem -Path $directory -Recurse -Force | Where-Object { $_.PSIsContainer -and (Get-ChildItem -Path $_.FullName -Recurse -Force | Where-Object { !$_.PSIsContainer }) -eq $null } | Remove-Item -Force -Recurse;
}


$users = Get-ChildItem C:\Users | where {$_ -notlike 'Public'};
foreach($u in $users){
$scope = Get-ChildItem $u.FullName | where {$_ -like 'Desktop' -or $_ -like 'Documents' -or $_ -like 'Music' -or $_ -like 'Pictures' -or $_ -like 'Videos' -or $_ -like 'Downloads'}
foreach($item in $scope) {
    FilterByDate -directory $item -limit (Get-Date).AddDays(-7)
    }
}